#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "commands.h"
#include "global.h"
#include "simulator.h"
#include "SLer.h"
#include "randomLineup.h"
#include "Fixtures.h"

extern void setKnoGamesOrd( short gameStage, group_t groups[GROUPNUM] );
extern void showKnockout();

extern user_t *users;
extern short usersNumber;
extern short currentUser;

extern team_t teams[TEAMNUM];
extern group_t groups[GROUPNUM];
extern group_t groupsBefKno[GROUPNUM];
extern game_t playoff[PLAYOFFGAMES];

extern formation_t allowedForms[ALLOWFORMS];

extern game_t *groupGames[3];
extern game_t *knockoutGames[5]; 
extern short gameStage;

							// Sorting Tables
static void sortTable()
{
	team_t *temp;
	short int i = 0, j = 0, t = 0;

	for (i = 0; i < 8; i++)
	{
		for (j = 0; j < 4; j++)
		{
			for (t = 0; t < 3; t++)
			{
				if (groups[i].gpTeams[t]->details.Pts < groups[i].gpTeams[t + 1]->details.Pts)
				{
					temp = groups[i].gpTeams[t];
					groups[i].gpTeams[t] = groups[i].gpTeams[t + 1];
					groups[i].gpTeams[t + 1] = temp;
				}
				else if (groups[i].gpTeams[t]->details.Pts == groups[i].gpTeams[t + 1]->details.Pts && groups[i].gpTeams[t]->details.GD < groups[i].gpTeams[t + 1]->details.GD)
				{
					temp = groups[i].gpTeams[t];
					groups[i].gpTeams[t] = groups[i].gpTeams[t + 1];
					groups[i].gpTeams[t + 1] = temp;
				}
				else if (groups[i].gpTeams[t]->details.Pts == groups[i].gpTeams[t + 1]->details.Pts && groups[i].gpTeams[t]->details.GD == groups[i].gpTeams[t + 1]->details.GD && groups[i].gpTeams[t]->details.GF < groups[i].gpTeams[t + 1]->details.GF)
				{
					temp = groups[i].gpTeams[t];
					groups[i].gpTeams[t] = groups[i].gpTeams[t + 1];
					groups[i].gpTeams[t + 1] = temp;
				}
			}
		}
	}
}

// is option
static bool isopt( char *commandPart )
{
	return commandPart[0] == '-' && strlen(commandPart) == 2;
}

static char getOption(  char **command, short commandLen )
{
	if( commandLen > 1 && isopt(command[1]) )
		return command[1][1];
	return '\0';
}

short lineupCommand( char **command, short commandLen )
{
	void printLine()
	{
		printf(GREEN);
		for (int j = 0; j < 66; j++)
			printf("%c",'-');
		printf(RESET);
	}


	team_t *t = currUserTeam(); 	// current team
	
	if( commandLen > 1 )
	{
		switch( getOption( command, commandLen ) )
		{
			case 'r':
				if( commandLen != 2 )
					return 1;
				else
					setRandomLineup( users[currentUser].team );
				break;
			case 'f':							// set formation
				if( commandLen != 5 )
					return 1;
				short form[3] = {(short)atoi(command[2]), (short)atoi(command[3]), (short)atoi(command[4])};
				if( !isTrueForm( form ) )
				{
					printf(RED" <This formation isn`t an allowed formation to select>\n"RESET);
					return 1;
				}
				t->lineup.formation = getFormationByForm( form );
				break;
			case 'c':							// change player
				if( commandLen != 4 )
					return 1;
				int source = 0, target = 0;
				source = atoi( command[2] );
				target = atoi( command[3] );
				if( source == target )
				{
					printf(RED" <Source ID and target ID should be different>\n"RESET);
					return 0;
				}
				if( source > t->playersNumber || source < 1 || target > t->playersNumber || target < 1 )
				{
					printf(RED" <Source ID or target ID is not exist>\n"RESET);
					return 0;
				}
				player_t *tmp = t->lineup.players[source-1];
				t->lineup.players[source-1] = t->lineup.players[target-1];
				t->lineup.players[target-1] = tmp;
				break;
			default:
				return 1;
		}
	}
	
	player_t *p; 							// current player
	short form[4];							// fomration ex. 4 3 3 and 7 for subsitution
	memcpy( form, t->lineup.formation->form, 3*sizeof(short) );
	form[3] = 7; 							// subsitution players
	playerPost_t post = GOALKEEPER;
	
	printLine();
	
	printf(GREEN"\n|"YELLOW" ID ");
	printf(GREEN"|"YELLOW" Name            	 ");
	printf(GREEN"|"YELLOW" Num ");
	printf(GREEN"|"YELLOW" Age ");
	printf(GREEN"|"YELLOW" Skl ");
	printf(GREEN"|"YELLOW" Fit ");
	printf(GREEN"|"YELLOW" Frm ");
	printf(GREEN"|"YELLOW" MP ");
	printf(GREEN"|"YELLOW" CP "GREEN"|\n"RESET);

	printLine();

	printf("\n");

	for( int i = 0; i < t->playersNumber; i++ )
	{
		p = t->lineup.players[i];

		printf(GREEN"|"MAGENTA" %02d ", i + 1);
		printf(GREEN"|"BLUE" %-17s ", p->name);
		printf(GREEN"|"RED"  %2d ", p->number);
		printf(GREEN"|"RED"  %02d ", p->age);
		printf(GREEN"|"RED"  %02d ", p->skill);
		printf(GREEN"|"RED"  %02d ", p->fitness);
		printf(GREEN"|"RED"  %2d ", p->form);
		printf(GREEN"|"RED"  %c ", pptoc(p->post));
		printf(GREEN"|"RED"  %c "GREEN"|\n"RESET, post > 4 ? ' ' : pptoc(post));

		if( post > 0 && post < 5 )						// dont run form goalkeeper
		{
			form[post-1]--;
			if( form[post-1] == 0 )
				post++;
		}
		else
			post++;										// after goalkeeper
	}

	printLine();
	printf(GREEN"\n|"CYAN);
	printf(" HELP:\t\t\t      MP = Main Post,  CP = Current Post "GREEN"|\n"RESET);
	printLine();
	printf("\n\n"); 

	return 0;
}

short proceedCommand( char **command, short commandLen )
{
	short int n;
	void recursive()
	{
		if( gameStage < 3 )
			printf(GREEN" <One stage done in group mode>\n"RESET);
		else if( gameStage < 8 )
			printf(GREEN" <One stage done in knockout mode>\n"RESET);
		else
		{
			printf(RED" <Cup has ended>\n"RESET);
			return;
		}
		
		game_t *games;

		if (gameStage > 2)
			games = knockoutGames[gameStage - 3];
		
		else
			games = groupGames[gameStage];

		game_t *onTimeGames = games;
		int j = 0;
		simulator(onTimeGames);
		onTimeGames = onTimeGames->simGame;

		while (onTimeGames != games)
		{
			simulator(onTimeGames);
			onTimeGames = onTimeGames->simGame;
		}
		
		gameStage++;
	}

	if (commandLen != 2)
		return 1;

	n = atoi(command[1]);
	
	if( n == 0 )
		return 1;

	while (n)
	{
		recursive();
		sortTable();
		if( gameStage == 3 )
			memcpy( groupsBefKno, groups, GROUPNUM * sizeof(group_t) );
		setKnoGamesOrd(gameStage, groups);
		n--;
	}

	return 0;
}

short tableCommand( char **command, short commandLen )
{
	void printLine()
	{
		printf(GREEN);
		for (int j = 0; j < 63; j++)
			printf("%c",'-');
		printf(RESET);
	}
	
    int i = 0, j = 0, select = 0;
	user_t *user = 0;
	printf(" Please tell us what stage you looking for by entering ID \n");
	printf(" 1. Group stage\n");
	printf(" 2. Knockout stage\n");
	printf("\tselect: ");
    scanf("%d", &select);
	if (select == 1)
	{
		for (i = 0; i < 8; i++)
		{
		    printLine();

		    printf(GREEN"\n|"YELLOW"Group %-17c", i + 'A');
		    printf(GREEN"|"YELLOW" MP "	);
		    printf(GREEN"|"YELLOW" W "	);
		    printf(GREEN"|"YELLOW" D "	);
		    printf(GREEN"|"YELLOW" L "	);
		    printf(GREEN"|"YELLOW" GF "	);
		    printf(GREEN"|"YELLOW" GA "	);
		    printf(GREEN"|"YELLOW" GD "	);
		    printf(GREEN"|"YELLOW" Pts "	GREEN"|\n"RESET);

		    printLine();
		    
		    for (j = 0; j < 4; j++)
		    {
		        printf(GREEN"\n|"BLUE" %-22s"GREEN"|", groups[i].gpTeams[j]->name);
		        printf(RED		"  %d "	GREEN"|", groups[i].gpTeams[j]->details.MP);
		        printf(RED		" %d "	GREEN"|", groups[i].gpTeams[j]->details.W);
		        printf(RED		" %d "	GREEN"|", groups[i].gpTeams[j]->details.D);
		        printf(RED		" %d "	GREEN"|", groups[i].gpTeams[j]->details.L);
		        printf(RED		" %2d "	GREEN"|", groups[i].gpTeams[j]->details.GF);
		        printf(RED		" %2d "	GREEN"|", groups[i].gpTeams[j]->details.GA);
		        printf(RED		" %2d "	GREEN"|", groups[i].gpTeams[j]->details.GD);
		        printf(MAGENTA	"  %d  "GREEN"|"RESET, groups[i].gpTeams[j]->details.Pts);
		        user = teamUser(groups[i].gpTeams[j]);
		        if( user != NULL )
					printf(" user:%s", user->username);
		    }
		    
		    printf("\n");

		    printLine();
		    
			printf("\n\n");
		}
	}
	else if (select == 2)
		showKnockout();
	else
		printf(RED" <Input was wrong. try again>"RESET);

    return 0;
}

short saveCommand( char **command, short commandLen )
{
	if( commandLen != 2 )
		return 1;
	
	saveGame(command[1]);
	printf(GREEN" <Game saved with profile name '%s'>\n"RESET, command[1]);
	return 0;
}

short switchCommand( char **command, short commandLen )
{
	if( commandLen != 3 )
		return 1; // commandLen error
	char *username = command[1];
	char *password = command[2];
	int index = userInd( username );
	if( index == -1 || strcmp(users[index].password, password) != 0 )
		printf(RED" <Username or Password is wrong!> \n"RESET);
	else
	{
		currentUser = index;
		printf(GREEN" <Switch to user %s done successfully>\n"RESET, username);
	}
	return 0;
}

short fixturesCommand( char **command, short commandLen )
{
	fixtures();
	return 0;
}

short helpCommand( char **command, short commandLen )
{
	return 1;
}
