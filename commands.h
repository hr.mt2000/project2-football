
short lineupCommand( char **command, short commandLen );
short proceedCommand( char **command, short commandLen );
short tableCommand( char **command, short commandLen );
short saveCommand( char **command, short commandLen );
short switchCommand( char **command, short commandLen );
short fixturesCommand( char **command, short commandLen );
short helpCommand( char **command, short commandLen );
