#ifndef true
#include <stdbool.h>
#endif
#ifndef _STDIO_H
#include <stdio.h>
#endif

typedef enum {
	CONCACAF,
	UEFA,
	AFC,
	CAF,
	CONMEBOL
} continent_t;

typedef enum{
	A,B,C,D,E,F,G,H
} groupName_t;

typedef enum{
	GOALKEEPER,
	DEFENDER,
	MIDFIELDER,
	ATTACKER,
	SUBSITUTION
} playerPost_t;

typedef struct{
    char *name;
    short number; // player number
    short skill;
    short fitness;
    short form;
    short age;
    playerPost_t post;
} player_t;

typedef struct {
	short form[3];
	short attPts; // attack point
	short defPts; // defense point
} formation_t;

typedef struct {
	formation_t *formation;
	player_t **players;
} teamLineup_t;

typedef struct {
	int MP;
	int W;
	int D;
	int L;
	int GF;
	int GA;
	int GD;
	int Pts;
} teamDetails_t;

typedef struct {
	const char *name;
    short playersNumber;
    short seed;
	continent_t continent;
    player_t *players;
    teamLineup_t lineup;
    teamDetails_t details;
} team_t;

typedef struct {
	char *username;
	char *password;
	team_t *team;
} user_t;

typedef struct {
    team_t *gpTeams[4];
} group_t;

typedef struct{
	short t1Goals; // team 1 goals number
	short t2Goals; // team 2 goals number
	/*short yCards1; // yellow cards number team 1
	short yCards2; // yellow cards number team 2
	short rCards1;
	short rCards2;*/
} gameResult_t;

typedef struct game{
	team_t *team1;
	team_t *team2;
	gameResult_t *result;
	struct game *simGame; // Simultaneous game // bazi ham zaman
} game_t;


#define TEAMNUM 32
#define GROUPNUM 8
#define PLAYOFFGAMES 31
#define ALLOWFORMS 8
#define GRPGMENUM 3 // group games number
#define KNKGMENUM 5 // knockout games numer


// Colors and font-styles
#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"
#define FBOLD   "\x1b[1m"

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

// set a random form from allowed formations for input array
formation_t *randForm();

/* [4,3,3],... is an allowed formation or not
 * allowed fromations configuration can be found in main.c global variables
 */
bool isTrueForm( short form[3] );

// playerPost to char ex. 1=DEFENDER => D
char pptoc( playerPost_t post );

// add group member ( this function adds member to a group )
void addgrpmem( group_t *group, team_t *team );

// get team index by its pointer. if not found return -1
int teamInd( team_t *t );

// get user index from username. if not found return -1
int userInd( char *username );

// team`s user - find user of a team
user_t *teamUser( team_t *team );

// returns current user`s team
team_t *currUserTeam();

// continent string name to continent_t enum
continent_t strtocont( const char *str );

// player post string to playerPost_t enum
playerPost_t strtopost( const char *str );

// get input string from fp file with variable length
char *inputString(FILE* fp);

// current player index in lineup of team
int currPlayerInd( team_t *t, player_t *p );

// current player post in lineup of team
playerPost_t currPlayerPost( team_t *t, player_t *p );

// return true if game is in kockoutGames
bool isKnockout( game_t *g );

// comment = function name
formation_t *getFormationByForm( short arr[3] );

// return winner of a kockout game
team_t *knockWinner( game_t *g );

// return loser of a kockout game
team_t *knockLoser( game_t *g );
