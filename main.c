#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "commands.h"
#include "global.h"
#include "draw.h"
#include "separator.h"
#include "SLer.h"
#include "setSFF.h"
#include "randomLineup.h"

user_t *users;
short usersNumber = 0;
short currentUser = -1; 							// which user is managing

team_t teams[TEAMNUM];
group_t drawedGroups[GROUPNUM]; // constant copy of groups // used for save and load
group_t groupsBefKno[GROUPNUM]; // groups before knockout games // used for save and load
group_t groups[GROUPNUM];

game_t *groupGames[GRPGMENUM];
game_t *knockoutGames[KNKGMENUM]; 							// marhale hazfi
short gameStage = 0;								// stage of games

void setGrpGamesOrd();
void setKnoGames();
void setKnoGamesOrd( short gameStage, group_t groups[GROUPNUM] );

formation_t allowedForms[ALLOWFORMS] = { 			// Allowed formations
//   form    atpts defpts
	{{3,4,3}, 55  , 100  },
	{{3,5,2}, 60  , 95   },
	{{4,4,2}, 70  , 80   },
	{{4,3,3}, 65  , 95   },
	{{4,5,1}, 80  , 70   },
	{{4,2,4}, 70  , 80   },
	{{5,3,2}, 90  , 65   },
	{{5,4,1}, 100 , 55   }
};

typedef struct {
	const char *command;
	short (*function)( char**, short );
	const char *help;
} command_t;
static command_t *commands;
static int commandsNumber;

command_t *newCommand( const char *command, short (*function)( char**, short ), const char *help )
{
	command_t *ret = 0;
	commandsNumber++;
	commands = realloc( commands, commandsNumber * sizeof(command_t) );
	if( commands == NULL )
		perror("Error in realloc for commands array in newCommand function");
	ret = &commands[commandsNumber-1];
	ret->command = command;
	ret->function = function;
	ret->help = help;
	return ret;
}

static void flush()
{
	while( getchar() != '\n' );
}

static bool isSelected( team_t *team )
{
	// search in all users to find team
	for( int i = 0; i < usersNumber; i++ )
	{
		if( users[i].team == team )
			return true;
	}
	return false;
}

static void printTeams()
{
	for( int i = 0; i < TEAMNUM; i++ )
	{
		char *selected = isSelected(&teams[i]) ? " (selected)" : "";
		printf(" %d. %s%s\n", i+1, teams[i].name, selected);
	}
}

static void startNewGame()
{
	int id = 0;									// team id that show to user
	short usersNumberLocal = 0;
	bool confirm = false;
	while( !confirm )
	{
		printf("\n Enter number of users (min=1, max=%d): ", TEAMNUM);
		scanf("%hu", &usersNumberLocal);
		if( usersNumberLocal > 0 && usersNumberLocal < 33 )
			confirm = true;
		flush();
	}
	users = calloc( usersNumberLocal, sizeof(user_t) );
	if( users == NULL )
		perror("Error in calloc for users array");
	for( int i = 0; i < usersNumberLocal; i++ )
	{
		confirm = false;
		
		char *input = 0;
		while( !confirm )
		{
			if( input != 0 )
				free(input);
			printf("\n Enter username for user %d: ", i+1);
			input = inputString(stdin);			// get username
			if( userInd(input) != -1 )
				printf(RED" <This username has been chosen by another user>\n"RESET);
			else if( strstr( input, " " ) != 0 )
				printf(RED" <You cant use ' '(space) in username>\n"RESET);
			else
				confirm = true;
				
		}
		users[i].username = input;
		confirm = false;
		
		input = 0;
		while( !confirm )
		{
			if( input != 0 )
				free(input);
			printf(" Enter password for user %d: ", i+1);
			input = inputString(stdin); 		// get pssword
			if( strstr( input, " " ) != 0 )
				printf(RED" <You cant use ' '(space) in username>\n"RESET);
			else
				confirm = true;
		}
		users[i].password = input;
		
		printTeams();
		while( users[i].team == NULL )			// until this user team has not set
		{
			printf(" Select a team for user %d (just enter id): ", i+1);
			scanf("%d", &id);
			flush();
			if( id < 1 || id > TEAMNUM )		// wrong input
			{
				printf("%s", RED" <Wrong input>\n"RESET);
				continue;
			}
			if( isSelected( &teams[id-1] ) )	// selected team
			{
				printf("%s", RED" <This team has been selected by another user>\n"RESET);
				continue;
			}
			users[i].team = &teams[id-1];		// set user team
		}
		usersNumber++;
	}
	currentUser = -1;
	
	/*Ask for random draw or not*/
	int select = 0;
	printf("\n Do you want to start game with world cup 2018 tables\n");
	while( select != 1 && select != 2 )			// until select 1 or 2
	{
		printf(" 1. No, please do a random draw \n");
		printf(" 2. Yes, set tables like world cup \n");
		printf("\tselect: ");
		scanf("%d", &select	);
		flush();
	}
	if( select == 1 )
		draw();
		
	setGrpGamesOrd();
	setKnoGames();
}
static void staticLoadGame() // it is static function of loadGame
{
	char *filename;
	printf(" Enter your save profile name: ");
	filename = inputString(stdin);
	while( loadGame(filename) )
	{
		printf(RED" <An error occurred in loading your game. maybe your entered profile name doesn`t exist. try again>\n"RESET);
		printf(" Enter your save profile name: ");
		filename = inputString(stdin);
	}
	printf(GREEN" <Your game loaded successfully>\n"RESET);
}

static void commandHandler( char **command, short commandLen )
{
	/*for( int i = 0; i < commandLen; i++ ) // debug
		printf("\n%s\n", command[i]);*/
	
	if( commandLen < 1 )
		return;
	
	if( commandLen == 1 && strcmp(command[0], "exit") == 0 )
		exit(0);
	
	short err = -1;
	
	
	if( commandLen == 2 && strcmp(command[1], "-h") == 0 ) // help for any command
	{
		for( int i = 0; i < commandsNumber; i++ )
		{
			if( strcmp(command[0], commands[i].command) == 0 )
			{
				printf( "\n"FBOLD"%s help:\n"RESET" %s\n", commands[i].command, commands[i].help );
				return;
			}
		}
	}
	
	
	for( int i = 0; i < commandsNumber; i++ )
	{
		// if currentUser has not set , user just can use switch and exit command
		if( currentUser == -1 && strcmp(command[0], "switch") != 0 && strcmp(command[0], "exit") != 0 )
			continue;
		
		if( strcmp(command[0], commands[i].command) == 0 )
		{
			err = commands[i].function( command, commandLen );
			if( err != 0 )
				printf( "\n"FBOLD"%s help:\n"RESET" %s\n", commands[i].command, commands[i].help );
			return;
		}
	}
	
	printf( "\n"FBOLD"commands help:\n"RESET" %s\n", commands[0].help );
}

// set group games order
void setGrpGamesOrd()
{
	static bool oneRun = false;
	if( oneRun )
		return;
	oneRun = true;
	
	const int STAGESNUM = 3; // we have (2*GROUPNUM) simultaneous games in each stage
	
	int order[6][2] = { {0,1}, {2,3}, {0,2}, {3,1}, {3,0}, {1,2} }; // group games order
	//simGames:			 (^   &  ^)	   (^   &  ^)	 (^   &  ^)
	//					 first stage   second stage  third stage
	int i = 0, j = 0;
	
	game_t *g1, *g2, *gme;
	for( int j = 0; j < STAGESNUM; j++ )
	{
		g1 = calloc( 1, sizeof(game_t) );
		g2 = NULL;
		gme = g1;
		int grpInd = 0, // group index
			ordInd = 0; // order index (related to order array)
		for( i = 0; i < 2 * GROUPNUM /*Each group have 2 simultaneous game*/; i++ )
		{
			grpInd = (int)(i/2); // each group have 2 game, so grpInd should increase after 2 game
			ordInd = j * 2 + i % 2; // in each stage we have 2 game for each group
			g1->team1 = drawedGroups[grpInd].gpTeams[ order[ordInd][0] ];
			g1->team2 = drawedGroups[grpInd].gpTeams[ order[ordInd][1] ];
			if( i != 2 * GROUPNUM - 1 ) // if it isn`t last
			{
				g2 = calloc( 1, sizeof(game_t) );
				g1->simGame = g2;
				g1 = g2;
				g2 = NULL;
			}
		}
		g1->simGame = gme;
		
		groupGames[j] = gme;
	}
}

// set knockout games order in one stage
void setKnoGamesOrd( short gameStage, group_t groups[GROUPNUM] )
{
	const int STAGESNUM = 5;
	game_t *lastStage = NULL,
		   *currStage = NULL;
	
	if( gameStage < 3 || gameStage > 6 )
		return;
	if( gameStage == 3 )
		lastStage = groupGames[2];
	else
		lastStage = knockoutGames[gameStage-1-3]; // 1 for last, 3 for knockout
	
	if( gameStage == 6 ) // if it is ranking game
	{
		/* set ranking game teams */
		knockoutGames[3]->team1 = knockLoser( lastStage ); 
		knockoutGames[3]->team2 = knockLoser( lastStage->simGame );
		
		currStage = knockoutGames[4]; // currStage = final
	}
	else
		currStage = knockoutGames[gameStage-3];

	game_t *cg = currStage, // current stage game
		   *lg = lastStage; // last stage game
	
	bool firstTime = true;
	bool firstTeam = true;
	int i = 0;
	while( cg != currStage || firstTime )
	{
		firstTime = false;
		if( gameStage == 3 )
		{
			if( firstTeam )
			{
				cg->team1 = groups[i++].gpTeams[0];
				cg->team2 = groups[i++].gpTeams[1];
			}
			else
			{
				cg->team1 = groups[i++].gpTeams[1];
				cg->team2 = groups[i++].gpTeams[0];
			}
			if( i >= 7 )
			{
				i = 0;
				firstTeam = false;
			}
		}
		else
		{
			cg->team1 = knockWinner( lg );
			lg = lg->simGame;
			cg->team2 = knockWinner( lg );
			lg = lg->simGame;
		}
		cg = cg->simGame;
	}
}

// set knockout games variables
void setKnoGames()
{
	const int STAGESNUM = 5;
	
	int i = 0, j = 0, k = 0;
	
	game_t *g1, *g2, *gme;
	for( j = 0, k = 1; j < STAGESNUM; j++, k *= 2 )
	{
		g1 = calloc( 1, sizeof(game_t) );
		g2 = NULL;
		gme = g1;
		int grpInd = 0; // group index // start from group A
		for( i = 0; i < GROUPNUM / k /*Each group have 2 simultaneous game and results have half of it*/; i++ )
		{
			if( i < GROUPNUM / k - 1 ) // if it isn`t last
			{
				g2 = calloc( 1, sizeof(game_t) );
				g1->simGame = g2;
				g1 = g2;
				g2 = NULL;
			}
		}
		g1->simGame = gme;
		
		knockoutGames[j] = gme;
	}
}

int main()
{
	/* set memory to zero */
	memset( teams, 0, sizeof(team_t) * TEAMNUM );
	commands = calloc( 1, sizeof(command_t) );

	srand(time(NULL));
	
	
	readfile();
	
	memcpy( drawedGroups, groups, sizeof(group_t) * GROUPNUM );
	
	setSFF();
	
	
	/* default values for variables */
	// default value for teamLineups of teams
	team_t *t;
	player_t *p;
	for( int i = 0; i < TEAMNUM; i++ )
	{
		t = &teams[i];
		t->lineup.formation = randForm();
		t->lineup.players = calloc( t->playersNumber, sizeof(player_t*) );
		for( int j = 0; j < t->playersNumber; j++ )
		{
			t->lineup.players[j] = &t->players[j];
		}
	}
	
	
	
	
	
	int select = 0;
	while( select != 1 && select != 2 )			// until select 1 or 2
	{
		printf(" 1. Start New Game \n");
		printf(" 2. Load Game \n");
		printf("%s", "\tselect: ");
		scanf("%d", &select	);
		flush();
	}
	if( select == 1 )
		startNewGame();
	else if( select == 2 )
		staticLoadGame();
	
	printf(YELLOW"\n Current user isn`t set. use switch command to switch to one user.\n"RESET);
	printf(YELLOW" Press enter to continue.\n"RESET);
	
	
	/* it was for debug
	int asdf = 0; 
	for( int i = 0; i < KNKGMENUM; i++ )
	{
		game_t *g1 = NULL;
		asdf = 0;
		printf(" -------------------------------------------------- ");
		while( g1 != knockoutGames[i] )
		{
			if( g1 == NULL )
				g1 = knockoutGames[i]->simGame;
			else
				g1 = g1->simGame;
			printf("\n%d) %d. %s VS %s\n", asdf, i, g1->team1, g1->team2);
			asdf++;
		}
	}*/
	
	
	/*set random lineup for all teams*/
	for( int i = 0; i < TEAMNUM; i++ )
		setRandomLineup( &teams[i] );
	
	
	newCommand( "help", helpCommand, "\n\
	This program have these commands to use (each command have a help for itself. you can see it by <command> -h):\n\
		"FBOLD"lineup:"RESET" manage team arrangement "RED"(*)"RESET"\n\
		"FBOLD"proceed:"RESET" run stage or stages of world-cup "RED"(*)"RESET"\n\
		"FBOLD"save:"RESET" save all your game data to a file "RED"(*)"RESET"\n\
		"FBOLD"fixtures:"RESET" show result of passed games and remaining games "RED"(*)"RESET"\n\
		"FBOLD"switch:"RESET" switch from a user to another user\n\
		"FBOLD"exit:"RESET" exit program (it is like ctrl-c)\n\
		"RED"(*)"RESET" these commands just can be used when you loged in as an user by use of switch command\n\
	" );
	newCommand( "lineup", lineupCommand, "\n\
	You can use lineup for view your team lineup and change it.\n\
		Show current lineup: lineup\n\
		"FBOLD"Change formation:"RESET" lineup -f <NumberOfDefenders> <NumberOfMidfielders> <NumberOfAttackers>\n\
			You can just use 3 4 3, 3 5 2, 4 4 2, 4 3 3, 4 5 1, 4 2 4, 5 3 2, 5 4 1 ...\n\
		"FBOLD"Change player position:"RESET" lineup -c <CurrentPosition> <TargetPosition>\n\
			CurrentPosition and TargetPosition is position numbers that is shown in lineup table (use lineup to see it)\n\
		"FBOLD"Automatic set lineup:"RESET" lineup -r\n\
	" );
	newCommand( "table", tableCommand, "\n\
	You can use table to viewing world cup 2018 table (points , advanced team ...)\n\
	" );
	newCommand( "proceed", proceedCommand, "\n\
	Proceed command start the first 2 maches in group stage or a round in knockout stage:\n\
		"FBOLD"procced <n>:"RESET" you should enter an argument for proceed that specify the number of stages that should pass (min=1).\n\
		"RED"(n is required)"RESET"\n\
	" );
	newCommand( "save", saveCommand, "\n\
	By saving your game, information save by name of your profile & anytime you want can resume the game\n\
		"FBOLD"Save:"RESET" save <profileName>\n\
		"YELLOW"The file will be save in current directory with name <profileName>.fpf and you can load it at game start by its <profileName>"RESET"\n\
		"RED"Repetitious profile name will be overwriten"RESET"\n\
	" );
	newCommand( "switch", switchCommand, "\n\
	Switch to another user:\n\
		switch <username> <password>\n\
		"RED"username and password are required"RESET"\n\
	" );
	newCommand( "fixtures", fixturesCommand, "\n\
	Fixtures show you the results of the games or games that have been not held\n\
		just type fixtures\n\
	" );
	
	
	// get commands and options from input
	char *nullString = "";
	int commandLen = 1;
	char **command = calloc( commandLen, sizeof(char *) );
	if( command == NULL )
		perror("Error in calloc for command array (in main function)");
	char c = 0;
	int i = 0;
	flush();
	while(1)									// get input command for ever
	{
		commandLen = 1;
		i = 0;
		char str[50];
		printf("%s", MAGENTA"-FootballWorldCup-> "RESET);
		while( 1 )								// get a part of command
		{
			c = getchar();
			if( c == ' ' || c == '\n' )			// new command or command part
			{
				if( strcmp( str, nullString ) != 0 )
				{
					command = realloc( command, commandLen * sizeof(char *) );
					command[commandLen-1] = calloc( i+2, sizeof(char) );
					if( command == NULL )
						perror("Error in calloc for command part of command array (in main function)");
					strncpy( command[commandLen-1], str, i );
					memset( str, 0, 50 );
					commandLen++;
				}
				else
					continue;
				if( c == '\n' )					// new command
					break;
				i = 0;
			}
			else
			{
				str[i] = c; 					// save command part
				i++;
			}
		}
		commandHandler( command, commandLen-1 );
		for( int i = 0; i < commandLen-1; i++ )
		{
			free(command[i]);
		}
	}
    

    return 0;
}
