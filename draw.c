#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "draw.h"
#include "global.h"

static bool checkPossible(short int grpConSize[][5], short int teamNum, short int groupNum);
static short int chooseTeamNum(bool teamsAv[][8], short int seedNum, short int step);
static short int chooseGpNum(short int groupNum, short int teamNum, short int grpConSize[][5], short int  grpSize[], short int seedNum);
static short int chooseGpPos(bool groupsAv[][4], short int groupNum, short int step);

extern team_t teams[TEAMNUM];
extern group_t groups[GROUPNUM];

static bool checkPossible(short int grpConSize[][5], short int teamNum, short int groupNum)
{
    continent_t continent = teams[teamNum].continent;
    if (continent == 1)
    {
        if (grpConSize[groupNum][continent] <= 1)
            return true;
    }
    else
    {
        if (grpConSize[groupNum][continent] < 1)
            return true;
    }
    return false;
}

static short int chooseTeamNum(bool teamsAv[][8], short int seedNum, short int step)
{
    short int random = rand() % (8 - step);
	short int counter = -1, i = 0;
	for (i = 0; i < 8; i++)
	{
		if (teamsAv[seedNum][i] == true)
			counter++;
		if (counter == random)
			break;
	}
    teamsAv[seedNum][i] = false;
    return i;
}

static short int chooseGpNum(short int groupNum, short int teamNum, short int grpConSize[][5], short int  grpSize[], short int seedNum)
{
    while (checkPossible(grpConSize, teamNum, groupNum) == false || grpSize[groupNum] != seedNum)
        groupNum++;
	
	grpConSize[groupNum][teams[teamNum].continent]++;
    return groupNum;
}

static short int chooseGpPos(bool groupsAv[][4], short int groupNum, short int step)
{
    short int random = rand() % (4 - step);
	short int counter = -1, i = 0;
	for (i = 0; i < 4; i++)
	{
		if (groupsAv[groupNum][i] == true)
			counter++;
		if (counter == random)
			break;
	}
    groupsAv[groupNum][i] = false;
    return i;
}

// do draw
void draw()
{
    srand(time(NULL));

    bool teamsAv[4][8];
    bool groupsPosAv[GROUPNUM][4];
    short int Seed[4][8];
    short int grpConSize[8][5];
	short int pos[4] = {0, 0, 0, 0};
    short int teamNum = 0, groupNum = 0, groupPos = 0;
    short int i = 0, j = 0; 

	short int grpSize[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	short int t = 0, counter = 0;

	//memset(teamsAv, true, 32);
	//memset(groupsPosAv, true, 32);
	memset(grpConSize, 0, 40 * sizeof(short int));
	memset(Seed, 0, 32 * sizeof(short int));

	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 8; j++)
		{
			teamsAv[i][j] = true;
			groupsPosAv[j][i] = true;
		}
	}

    for (i = 0; i < TEAMNUM; i++)
    {
		Seed[teams[i].seed - 1][pos[teams[i].seed - 1]] = i;
		pos[teams[i].seed - 1]++;
    }

    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 8; j++)
        {
			t = 0;
			while (grpSize[t] != i)
				t++;			

            teamNum = Seed[i][chooseTeamNum(teamsAv, i, j)];
            groupNum = chooseGpNum(t, teamNum, grpConSize, grpSize, i);
			grpSize[groupNum]++;
            groupPos = chooseGpPos(groupsPosAv, groupNum, i);
            groups[groupNum].gpTeams[groupPos] = &teams[teamNum];
        }
    }
}

/// Description
/*

teamsAV 		=	teams availability
groupPosAv 		=	group position availalbility
grpConSize		=	group continent size
teamNum 		=	team number
groupNum 		=	group number
groupPos 		=	group Position
checkPossible 	= 	check team is possible or not
chooseTeamNum 	= 	choose team number
chooseGpNum 	= 	choose group number
chooseGpPos 	= 	choose group Position

*/
