#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "global.h"

extern team_t teams[TEAMNUM];
extern user_t *users;
short usersNumber ;
extern group_t drawedGroups[GROUPNUM];
extern group_t groupsBefKno[GROUPNUM];
extern group_t groups[GROUPNUM];

extern game_t *groupGames[3];
extern game_t *knockoutGames[5]; 
extern short gameStage;

extern void setGrpGamesOrd();
extern void setKnoGames();
extern void setKnoGamesOrd( short gameStage, group_t groups[GROUPNUM] );

void saveGame (char* Pro)
{
	char * ProfileName = calloc (strlen (Pro) + 5,sizeof(char));	 //bara esme profile hafeze migirim
	strcpy(ProfileName, Pro);
	strcat(ProfileName ,".fpf");			 // pasvand behesh midim
	FILE * Teams ;
	Teams = fopen (ProfileName ,"wb");		 
	for ( int i = 0 ; i < 32 ; i++)	 // etelate Players
	{
		for (int j = 0; j < teams[i].playersNumber ; j++)
		{
			fwrite(&teams[i].players[j].age, sizeof(short), 1, Teams);

			fwrite(&teams[i].players[j].fitness, sizeof(short), 1, Teams);
		
			fwrite(&teams[i].players[j].form, sizeof(short), 1, Teams);
		}
	}
	for (int i = 0 ; i < 32 ; i++)
	{
		fwrite(&teams[i].details, sizeof(teamDetails_t), 1, Teams);	// etelate team.Details
	}

	for ( int i = 0 ; i < 8 ; i++)
	{
		for(int j = 0 ; j < 4 ; j++)
		{
			int indexT = teamInd(groups[i].gpTeams[j]) ;

			fwrite(&indexT, sizeof(int), 1, Teams);
		}
	}

	fwrite(&usersNumber,sizeof(short),1,Teams);			 // tedad bazikonaye barname
	for ( int i = 0 ; i < usersNumber ; i++ )
	{
		int sizeOfName = strlen (users[i].username) + 1 ;		 

		fwrite (&sizeOfName ,sizeof(int),1,Teams);			 //andaze esmesh

		fwrite(users[i].username, sizeOfName, 1, Teams);			//khode esmesh

		int sizeOfPass = strlen(users[i].password) + 1;

		fwrite(&sizeOfPass, sizeof(int), 1, Teams);			//andaze pass

		fwrite(users[i].password, sizeOfPass, 1, Teams);			// khode pass

		int indexT = teamInd(users[i].team); // teami k entekhab kardan

		fwrite(&indexT, sizeof(int), 1, Teams);
	}
	
	
	/*save results of the games*/
	fwrite(&gameStage, sizeof(short), 1, Teams);
	// save drawedGroups
	for ( int i = 0 ; i < 8 ; i++)
	{
		for(int j = 0 ; j < 4 ; j++)
		{
			int indexT = teamInd(drawedGroups[i].gpTeams[j]) ;		 //jayegahe team ha pas az draw

			fwrite(&indexT, sizeof(int), 1, Teams);
		}
	}
	// save groupsBefKno
	for ( int i = 0 ; i < 8 ; i++)
	{
		for(int j = 0 ; j < 4 ; j++)
		{
			int indexT = teamInd(groupsBefKno[i].gpTeams[j]) ;		 //jayegahe team ha pas az draw

			fwrite(&indexT, sizeof(int), 1, Teams);
		}
	}
	// save results
	for( int i = 0; i < gameStage; i++ )
	{
		game_t *g1 = NULL, *gme = NULL;
		if( i < 3 )
			gme = groupGames[i];
		else
			gme = knockoutGames[i-3];
		g1 = gme;
		while(1)
		{
			fwrite(g1->result, sizeof(gameResult_t), 1, Teams);
			g1 = g1->simGame;
			if( g1 == gme )
				break;
		}
	}

	fclose(Teams);
}	 

	
bool loadGame (char* Pro)
{
	char * ProfileName = calloc (strlen (Pro) + 5,sizeof(char));
	strcpy (ProfileName , Pro ) ; 
	strcat(ProfileName ,".fpf");				//esm profile ro migire
	FILE * Teams ;
	Teams = fopen (ProfileName ,"rb");
	if (Teams == NULL)										 // age vojood nadasht return 1
	{
		return 1 ;
	}
	else
	{
		for ( int i = 0 ; i < 32 ; i++)
		{
			for (int j = 0; j < teams[i].playersNumber ; j++)
			{
				fread(&teams[i].players[j].age, sizeof(short), 1, Teams);

				fread(&teams[i].players[j].fitness, sizeof(short), 1, Teams);
				
				fread(&teams[i].players[j].form, sizeof(short), 1, Teams);					 // etelate players ro mikhone mirize to motoghayer
			}
		}

		for (int i = 0 ; i < 32 ; i++)
		{
			fread(&teams[i].details, sizeof(teamDetails_t), 1, Teams);			// teams.Detail
		}

		for ( int i = 0 ; i < 8 ; i++)
		{
			for(int j = 0 ; j < 4 ; j++)
			{
				int indexT;
				fread(&indexT, sizeof(int), 1, Teams);
				groups[i].gpTeams[j] = &teams[indexT];					// jayeahe team haro moshakhas mikone
			}
		}
		fread(&usersNumber, sizeof(short), 1, Teams);					 //tedad bazikonan
		users = calloc( usersNumber, sizeof(user_t) );
		
		users = calloc(usersNumber, sizeof(user_t));

		for(int i = 0 ; i < usersNumber ; i++)
		{
			int sizeOfName ;

			fread(&sizeOfName , sizeof(int) , 1 ,Teams);

			users[i].username = calloc(sizeOfName, sizeof(char));		 //hafeze bara esmeshoon

			fread(users[i].username, sizeOfName, 1, Teams);

			int sizeOfPass;

			fread(&sizeOfPass, sizeof(int), 1, Teams);

			users[i].password = calloc(sizeOfPass, sizeof(char));			//hafeze bara pass

			fread(users[i].password, sizeOfPass, 1, Teams);

			int indexT;

			fread(&indexT, sizeof(int), 1, Teams);					// team entekhabishoon

			users[i].team = &teams[indexT];
		}
	}
	
	/*load results of the games*/
	fread(&gameStage, sizeof(short), 1, Teams);
	
	// load drawedGroups and set groupGames
	for ( int i = 0 ; i < 8 ; i++)
	{
		for(int j = 0 ; j < 4 ; j++)
		{
			int indexT;
			fread(&indexT, sizeof(int), 1, Teams);
			drawedGroups[i].gpTeams[j] = &teams[indexT];					// jayeahe team haro moshakhas mikone
		}
	}
	setGrpGamesOrd();
	setKnoGames();
	
	// load groupsBefKno
	for ( int i = 0 ; i < 8 ; i++)
	{
		for(int j = 0 ; j < 4 ; j++)
		{
			int indexT;
			fread(&indexT, sizeof(int), 1, Teams);
			groupsBefKno[i].gpTeams[j] = &teams[indexT];					// jayeahe team haro moshakhas mikone
		}
	}
	
		
	// read results
	for( int i = 0; i < gameStage; i++ )
	{
		int resultsNum = 0;
		switch( i )
		{
			case 0:
			case 1:
			case 2:
				resultsNum = 2 * GROUPNUM;
				break;
			case 3:
				resultsNum = 8;
				break;
			case 4:
				resultsNum = 4;
				break;
			case 5:
				resultsNum = 2;
				break;
			case 6:
				resultsNum = 1;
				break;
			case 7:
				resultsNum = 1;
				break;
		}
		game_t *g1 = NULL;
		if( i < 3 )
			g1 = groupGames[i];
		else
			g1 = knockoutGames[i-3];
		for( int j = 0; j < resultsNum; j++ )
		{
			gameResult_t *result = calloc( 1, sizeof(gameResult_t) );
			fread(result, sizeof(gameResult_t), 1, Teams);
			g1->result = result;
			g1 = g1->simGame;
		}
		setKnoGamesOrd(i+1, groupsBefKno);
	}
	
	fclose(Teams);
	return 0 ;
}
