#include "global.h"
#include <string.h>
void printcup();
extern game_t *knockoutGames[KNKGMENUM];
static const char *a[31];
void showKnockout()
{
    int i = 0 ;
    char tbd[4] = "TBD";
    game_t *onGame = knockoutGames[0];
    for ( i = 0 ; i<32 ; i++)
    {
      a[i] = tbd;
    }
    if (knockoutGames[0]->team1 == NULL)
    {
      printcup();
      return ;
    }
    else
    {
      for ( i = 0; i < 16; i++) 
      {
        a[i] = onGame->team1->name;
        i++;
        a[i] = onGame->team2->name;
        onGame = onGame->simGame;
      }
      if(knockoutGames[1]->team1 == NULL)
      {
        printcup();
        return;
      }
      else
      {
        game_t *onGame = knockoutGames[1];
        for (i = 16; i < 24; i++) 
        {
          a[i] = onGame->team1->name;
          i++ ;
          a[i] = onGame->team2->name;
          onGame = onGame->simGame;
        }
        if (knockoutGames[2]->team1 == NULL) 
        {
          printcup();
          return;
        }
        else
        {
          game_t *onGame = knockoutGames[2];
          for (i = 24; i < 28; i++) 
          {
            a[i] = onGame->team1->name;
            i++;
            a[i] = onGame->team2->name;
            onGame = onGame->simGame;
          }
          if (knockoutGames[3]->team1 == NULL) 
          {
            printcup();
            return;
          }
          else
          {
            a[28] = knockoutGames[4]->team1->name;
            a[29] = knockoutGames[4]->team2->name;
            if (knockoutGames[4]->result == NULL)
            {
              printcup();
              return;
            }
            else
            {
              if (knockoutGames[4]->result->t1Goals > knockoutGames[4]->result->t2Goals)
              {
                a[30] = knockoutGames[4]->team1->name;
                printcup();
                return;
              }
              else
              {
                a[30] = knockoutGames[4]->team2->name;
                printcup();
                return;
              }
            }
          }
        }
      }
    }     
}
void printcup()
{
printf("\n\n\
----------------\n\
|"BLUE"%-14s"GREEN"|---\n\
----------------  |\n\
                  |   ----------------\n\
                  |---|"BLUE"%-14s"GREEN"|---\n\
                  |   ----------------  |\n\
                  |                     |\n\
----------------  |                     |\n\
|"BLUE"%-14s"GREEN"|---                     |\n\
----------------                        |\n\
                                        |\n\
                                        |   ----------------\n\
                                        |---|"BLUE"%-14s"GREEN"|---\n\
                                        |   ----------------  |\n\
                                        |                     |\n\
                                        |                     |\n\
----------------                        |                     |\n\
|"BLUE"%-14s"GREEN"|---                     |                     |\n\
----------------  |                     |                     |\n\
                  |   ----------------  |                     |\n\
                  |---|"BLUE"%-14s"GREEN"|---                     |\n\
                  |   ----------------                        |\n\
----------------  |                                           |\n\
|"BLUE"%-14s"GREEN"|---                                           |\n\
----------------                                              |\n\
                                                              |\n\
                                                              |\n\
                                                              |   ----------------\n\
                                                              |---|"BLUE"%-14s"GREEN"|---\n\
                                                              |   ----------------  |\n\
                                                              |                     |\n\
                                                              |                     |\n\
----------------                                              |                     |\n\
|"BLUE"%-14s"GREEN"|---                                           |                     |\n\
----------------  |                                           |                     |\n\
                  |   ----------------                        |                     |\n\
                  |---|"BLUE"%-14s"GREEN"|---                     |                     |\n\
                  |   ----------------  |                     |                     |\n\
----------------  |                     |                     |                     |\n\
|"BLUE"%-14s"GREEN"|---                     |                     |                     |\n\
----------------                        |                     |                     |\n\
                                        |                     |                     |\n\
                                        |   ----------------  |                     |\n\
                                        |---|"BLUE"%-14s"GREEN"|---                     |\n\
                                        |   ----------------                        |\n\
                                        |                                           |\n\
----------------                        |                                           |\n\
|"BLUE"%-14s"GREEN"|---                     |                                           |\n\
----------------  |                     |                                           |\n\
                  |   ----------------  |                                           |\n\
                  |---|"BLUE"%-14s"GREEN"|---                                           |\n\
                  |   ----------------                                              |\n\
----------------  |                                                                 |\n\
|"BLUE"%-14s"GREEN"|---                                                                 |\n\
----------------                                                                    |\n\
                                                                                    |\n\
                                                                                    |\n\
                                                                                    |   ----------------\n\
                                                                                    |---|"BLUE"%-14s"GREEN"|\n\
                                                                                    |   ----------------\n\
                                                                                    |\n\
                                                                                    |\n\
                                                                                    |\n\
                                                                                    |\n\
----------------                                                                    |\n\
|"BLUE"%-14s"GREEN"|---                                                                 |\n\
----------------  |                                                                 |\n\
                  |   ----------------                                              |\n\
                  |---|"BLUE"%-14s"GREEN"|---                                           |\n\
                  |   ----------------                                              |\n\
                  |                     |                                           |\n\
----------------  |                     |                                           |\n\
|"BLUE"%-14s"GREEN"|---                     |                                           |\n\
----------------                        |                                           |\n\
                                        |                                           |\n\
                                        |   ----------------                        |\n\
                                        |---|"BLUE"%-14s"GREEN"|---                     |\n\
                                        |   ----------------  |                     |\n\
                                        |                     |                     |\n\
                                        |                     |                     |\n\
----------------                        |                     |                     |\n\
|"BLUE"%-14s"GREEN"|---                     |                     |                     |\n\
----------------  |                     |                     |                     |\n\
                  |   ----------------  |                     |                     |\n\
                  |---|"BLUE"%-14s"GREEN"|---                     |                     |\n\
                  |   ----------------                        |                     |\n\
----------------  |                                           |                     |\n\
|"BLUE"%-14s"GREEN"|---                                           |                     |\n\
----------------                                              |                     |\n\
                                                              |                     |\n\
                                                              |                     |\n\
                                                              |   ----------------  |\n\
                                                              |---|"BLUE"%-14s"GREEN"|---\n\
                                                              |   ----------------\n\
                                                              |\n\
                                                              |\n\
----------------                                              |\n\
|"BLUE"%-14s"GREEN"|---                                           |\n\
----------------  |                                           |\n\
                  |   ----------------                        |\n\
                  |---|"BLUE"%-14s"GREEN"|---                     |\n\
                  |   ----------------  |                     |\n\
----------------  |                     |                     |\n\
|"BLUE"%-14s"GREEN"|---                     |                     |\n\
----------------                        |                     |\n\
                                        |                     |\n\
                                        |   ----------------  |\n\
                                        |---|"BLUE"%-14s"GREEN"|---\n\
                                        |   ----------------\n\
                                        |\n\
----------------                        |\n\
|"BLUE"%-14s"GREEN"|---                     |\n\
----------------  |                     |\n\
                  |   ----------------  |\n\
                  |---|"BLUE"%-14s"GREEN"|---\n\
                  |   ----------------\n\
----------------  |\n\
|"BLUE"%-14s"GREEN"|---\n\
----------------\n\n"RESET, a[0], a[16], a[1], a[24], a[2], a[17], a[3], a[28], a[4], a[18], a[5], a[25], a[6], a[19], a[7], a[30], a[8], a[20], a[9], a[26], a[10], a[21], a[11], a[29], a[12], a[22], a[13], a[27], a[14], a[23], a[15]);

}

