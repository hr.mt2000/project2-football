#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "global.h"

extern team_t teams[TEAMNUM];
extern group_t groups[GROUPNUM];

static char** Str_Split (char* , const char );

static char* concat(const char *s1, const char *s2)
{
    char *result = malloc(strlen(s1)+strlen(s2)+1);
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

void readfile() {
	FILE *TFile;
	FILE *PFile;
	char **Tokens;
	char *directory = "./info/";
	char Linestring[100];
	char Playerstring[100];
	int j = 0 , x = 0 , i = 0 ;
	char *addr = concat(directory, "Teams.csv");
	TFile = fopen(addr, "r");
	free(addr);
	if (TFile == NULL)
		perror("Error opening team file");
	else {
		for (i = 0; i < TEAMNUM; i++) 	
		{
			if (fgets(Linestring, 100, TFile) != NULL) {
				j = 0;
				Linestring[strlen(Linestring) - 1] = '\0';

				Tokens = Str_Split(Linestring, ',');

				char Dir[100];
				memset( Dir, 0, sizeof(char) * 100 );
				for (x = 0; Tokens[5][x] != '\0'; x++) {
					Dir[x] = Tokens[5][x];
				}

				teams[i].name = *(Tokens);
				teams[i].continent = strtocont( *(Tokens + 3) );
				teams[i].seed = atoi(*(Tokens + 4));
				groups[*(Tokens + 1)[0]-65].gpTeams[atoi(*(Tokens + 2))-1] = &teams[i];

				char *addr = concat(directory, Dir);
				PFile = fopen( addr , "r"); 
				free(addr);
				if (PFile == NULL)
					perror("Error opening player file");
				else 
				{
					for (int Pc = 0;fgets(Playerstring, 100, PFile) != NULL ; Pc ++) 
					{
						Playerstring[strlen(Playerstring) - 1] = '\0';
						Tokens = Str_Split(Playerstring, ',');

						teams[i].playersNumber++;
						player_t player;
						player.number = atoi(*(Tokens));
						player.name = *(Tokens + 1);
						player.age = atoi(*(Tokens + 2));
						player.post = strtopost( *(Tokens + 3) );
						teams[i].players = realloc( teams[i].players, teams[i].playersNumber * sizeof(player_t) );
						teams[i].players[teams[i].playersNumber-1] = player;
						
					}
				}
				fclose(PFile);
			}
		}
	}

	fclose(TFile);
}

char** Str_Split(char* a_str, const char a_delim)
{
	char** result    = 0;
	size_t count     = 0;
	char* tmp        = a_str;
	char* last_comma = 0;
	char delim[2];
	delim[0] = a_delim;
	delim[1] = 0;

	/* Count how many elements will be extracted. */
	while (*tmp)
	{
		if (a_delim == *tmp)
		{
			count++;
			last_comma = tmp;
		}
		tmp++;
	}

	/* Add space for trailing token. */
	count += last_comma < (a_str + strlen(a_str) - 1);

	/* Add space for terminating null string so caller
	knows where the list of returned strings ends. */
	count++;

	result = (char**)calloc(sizeof(char*) , count);
	//result = (char**)realloc(result , sizeof(char*) * count);

	if (result)
	{
		size_t idx  = 0;
		char* token = strtok(a_str, delim);

		while (token)
		{
			assert(idx < count);
			*(result + idx++) = strdup(token);
			token = strtok(0, delim);
		}
		assert(idx == count - 1);
		*(result + idx) = 0;
	}
	//  int p = sizeof(result) / sizeof(result[0]) - 1 ;
	//    result[p][strlen(result[p]) - 1] = '\0';
	return result;
}

