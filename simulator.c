#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include "global.h"
#include "randomLineup.h"

extern team_t teams[TEAMNUM];

static team_t *teamA;
static team_t *teamB;

int remainingTime = 90;

static int getTeamAttackScore(team_t *a)
{
    int midNum = a->lineup.formation->form[1], attNum = a->lineup.formation->form[2];
    int i = 0, attAvg = 0, midAvg = 0;
    int attPts = 0, skl = 0;
    for (i = 0; i < attNum; i++)
	{
		skl = a->lineup.players[10 - i]->skill;
		skl = skl * a->lineup.players[10 - i]->fitness / 100;
        attAvg += skl / (fabs(currPlayerPost(a, a->lineup.players[10 - i]) - (a->lineup.players[10 - i]->post)) + 1);
	}

    for (; i < midNum + attNum; i++)
	{
		skl = a->lineup.players[10 - i]->skill;
		skl = skl * a->lineup.players[10 - i]->fitness / 100;
        midAvg += skl / (fabs(currPlayerPost(a, a->lineup.players[10 - i]) - (a->lineup.players[10 - i]->post)) + 1);
	}

    attAvg /= attNum;
    midAvg /= midNum;

    attPts = (3 * attAvg + midAvg) / 4;
    attPts += a->lineup.formation->attPts * attPts / 100;

    return attPts;
}

static int getTeamDefenceScore(team_t *a)
{
    int midNum = a->lineup.formation->form[1], defNum = a->lineup.formation->form[0];
    int i = 0, defAvg = 0, midAvg = 0, goalAvg = 0;
    int defPts = 0, skl = 0;
    for (i = 0; i < defNum; i++)
	{
		skl = a->lineup.players[i + 1]->skill;
		skl = skl * a->lineup.players[i + 1]->fitness / 100;
        defAvg += skl / (fabs(currPlayerPost(a, a->lineup.players[i + 1]) - (a->lineup.players[i + 1]->post)) + 1);
	}

    for (; i < midNum + defNum; i++)
	{
		skl = a->lineup.players[i + 1]->skill;
		skl = skl * a->lineup.players[i + 1]->fitness / 100;
        midAvg += skl / (fabs(currPlayerPost(a, a->lineup.players[i + 1]) - (a->lineup.players[i + 1]->post)) + 1);
	}
	
	skl = a->lineup.players[0]->skill;
	skl = skl * a->lineup.players[0]->fitness / 100;
    goalAvg = skl / (fabs(currPlayerPost(a, a->lineup.players[0]) - (a->lineup.players[0]->post)) + 1);

    defAvg /= defNum;
    midAvg /= midNum;

    defPts = (4 * defAvg + midAvg + 2 * goalAvg) / 7;
    defPts += a->lineup.formation->defPts * defPts / 100;

    return defPts;
}

static void partSimulator(gameResult_t *result)      //need to complete
{
    short int offAdv = getTeamAttackScore(teamA) - getTeamDefenceScore(teamB);
    short int bonusPercent = 0, v1 = 0, i = 0;

    if (remainingTime <= 15)
        bonusPercent = 5;

/// goal possibility for team a
    if (offAdv <= 3 && offAdv >= -5)      // 10%
    {
        v1 = rand() % 100;
        if (v1 >= 90 - bonusPercent)
            result->t1Goals++;
    }
    else if (offAdv <= -5)               // 1%
    {
        v1 = rand() % 100;
        if (v1 >= 99 - bonusPercent)
            result->t1Goals++;
    }
    else if (offAdv <= 10 && offAdv >= 3) // 20%
    {
        v1 = rand() % 100;
        if (v1 >= 70 - bonusPercent)
            result->t1Goals++;
    }
    else if (offAdv >= 10)               // 40%
    {
        v1 = rand() % 100;
        if (v1 >= 50 - bonusPercent)
            result->t1Goals++;
    }

    offAdv = getTeamAttackScore(teamB) - getTeamDefenceScore(teamA);

/// goal possibility for team b
    if (offAdv <= 3 && offAdv >= -5)      // 10%
    {
        v1 = rand() % 100;
        if (v1 >= 90 - bonusPercent)
            result->t2Goals++;
    }
    else if (offAdv <= -5)               // 1%
    {
        v1 = rand() % 100;
        if (v1 >= 99 - bonusPercent)
            result->t2Goals++;
    }
    else if (offAdv <= 10 && offAdv >= 3) // 20%
    {
        v1 = rand() % 100;
        if (v1 >= 70 - bonusPercent)
            result->t2Goals++;
    }
    else if (offAdv >= 10)               // 40%
    {
        v1 = rand() % 100;
        if (v1 >= 50 - bonusPercent)
            result->t2Goals++;
    }

	for (i = 0; i < 11; i++)
        teamA->lineup.players[i]->fitness--;
	for (i = 0; i < 11; i++)
        teamB->lineup.players[i]->fitness--;

    remainingTime -= 15;
}

void penaltySim(game_t *games)
{
	int i = 0, diffForm = 0, v1 = 0;
	for (i = 0; i < 5; i++)
	{
		diffForm = teamA->players[10 - i].form - teamB->players[0].form;
		if (diffForm < 2 && diffForm > -2)		// 50%
		{
			v1 = rand() % 2;
			if (v1 == 1)
			{
				games->result->t1Goals++;
				teamA->players[10 - i].form++;
			}
			else
				teamB->players[0].form++;
		}
		else if (diffForm > 2)					// 75%
		{
			v1 = rand() % 4;
			if (v1 >=1)
			{
				games->result->t1Goals++;
				teamA->players[10 - i].form++;
			}
			else
				teamB->players[0].form++;
		}
		else if (diffForm < -2)					// 25%
		{
			v1 = rand() % 4;
			if (v1 == 0)
			{
				games->result->t1Goals++;
				teamA->players[10 - i].form++;
			}
			else
				teamB->players[0].form++;
		}

		diffForm = teamB->players[10 - i].form - teamA->players[0].form;
		if (diffForm < 2 && diffForm > -2)		// 50%
		{
			v1 = rand() % 2;
			if (v1 == 1)
			{
				games->result->t1Goals++;
				teamB->players[10 - i].form++;
			}
			else
				teamA->players[0].form++;
		}
		else if (diffForm > 2)					// 75%
		{
			v1 = rand() % 4;
			if (v1 >=1)
			{
				games->result->t1Goals++;
				teamB->players[10 - i].form++;
			}
			else
				teamA->players[0].form++;
		}
		else if (diffForm < -2)					// 25%
		{
			v1 = rand() % 4;
			if (v1 == 0)
			{
				games->result->t1Goals++;
				teamB->players[10 - i].form++;
			}
			else
				teamA->players[0].form++;
		}
	}

	while (games->result->t1Goals == games->result->t2Goals)
	{
		if (i == 11)
			i = 0;

		diffForm = teamA->players[10 - i].form - teamB->players[0].form;
		if (diffForm < 2 && diffForm > -2)		// 50%
		{
			v1 = rand() % 2;
			if (v1 == 1)
			{
				games->result->t1Goals++;
				teamA->players[10 - i].form++;
			}
			else
				teamB->players[0].form++;
		}
		else if (diffForm > 2)					// 75%
		{
			v1 = rand() % 4;
			if (v1 >=1)
			{
				games->result->t1Goals++;
				teamA->players[10 - i].form++;
			}
			else
				teamB->players[0].form++;
		}
		else if (diffForm < -2)					// 25%
		{
			v1 = rand() % 4;
			if (v1 == 0)
			{
				games->result->t1Goals++;
				teamA->players[10 - i].form++;
			}
			else
				teamB->players[0].form++;
		}

		diffForm = teamB->players[10 - i].form - teamA->players[0].form;
		if (diffForm < 2 && diffForm > -2)		// 50%
		{
			v1 = rand() % 2;
			if (v1 == 1)
			{
				games->result->t1Goals++;
				teamB->players[10 - i].form++;
			}
			else
				teamA->players[0].form++;
		}
		else if (diffForm > 2)					// 75%
		{
			v1 = rand() % 4;
			if (v1 >=1)
			{
				games->result->t1Goals++;
				teamB->players[10 - i].form++;
			}
			else
				teamA->players[0].form++;
		}
		else if (diffForm < -2)					// 25%
		{
			v1 = rand() % 4;
			if (v1 == 0)
			{
				games->result->t1Goals++;
				teamB->players[10 - i].form++;
			}
			else
				teamA->players[0].form++;
		}
		i++;
	}
}

void simulator (game_t *games)
{
    remainingTime = 90;

    teamA = games->team1;
    teamB = games->team2;
    games->result = calloc(1, sizeof(gameResult_t));

	int i = 0, j = 0;
    while(1)
    {
        partSimulator(games->result);
        if (remainingTime == 0)
            break;
    }
														// changing of fitness and form
	for (i = 0; i < 11; i++)	//Main players
    {
        teamA->lineup.players[i]->form++;
        teamB->lineup.players[i]->form++;
    }

	for (; i < 18; i++)			//Sub players
    {
        teamA->lineup.players[i]->fitness += 5;
        teamA->lineup.players[i]->form--;
        teamB->lineup.players[i]->fitness += 5;
        teamB->lineup.players[i]->form--;
    }

	for (j = i; j < teamA->playersNumber; j++)
    {
        teamA->lineup.players[i]->fitness += 5;
        teamA->lineup.players[i]->form -= 2;
    }

	for (; i < teamB->playersNumber; i++)
    {
        teamB->lineup.players[i]->fitness += 5;
        teamB->lineup.players[i]->form -= 2;
    }
															// ExtraTime and penalty
	if (isKnockout(games) && games->result->t1Goals == games->result->t2Goals)
	{

        remainingTime = 30;

        while(1)
        {
            partSimulator(games->result);
            if (remainingTime == 0)
                break;
        }

		if (games->result->t1Goals == games->result->t2Goals)
			penaltySim(games);

							// changing of fitness and form
		for (i = 0; i < 11; i++)
			teamA->lineup.players[i]->fitness -= 5;

		for (i = 0; i < 11; i++)
			teamB->lineup.players[i]->fitness -= 5;
	}
															// Increase Form of winner
	if (games->result->t1Goals > games->result->t2Goals)
	{
		if(!isKnockout(games))
		{
			teamA->details.W++;
			teamB->details.L++;
		}

		for (i = 0; i < teamA->playersNumber; i++)
		    teamA->players[i].form++;
	}

	else if (games->result->t1Goals < games->result->t2Goals)
	{
		if(!isKnockout(games))
		{
			teamB->details.W++;
			teamA->details.L++;
		}

		for (i = 0; i < teamB->playersNumber; i++)
		    teamB->players[i].form++;
	}

	else
	{
		if(!isKnockout(games))
		{
			teamA->details.D++;
			teamB->details.D++;
		}
	}

	for (i = 0; i < teamA->playersNumber; i++)
        if (teamA->players[i].fitness > 99)
			teamA->players[i].fitness = 99;
    
	for (i = 0; i < teamB->playersNumber; i++)
        if (teamB->players[i].fitness > 99)
			teamB->players[i].fitness = 99;

	for (i = 0; i < teamA->playersNumber; i++)
        if (teamA->players[i].form > 15)
			teamA->players[i].form = 15;
    
	for (i = 0; i < teamB->playersNumber; i++)
        if (teamB->players[i].form > 15)
			teamB->players[i].form = 15;
													// saving result to table

	if(!isKnockout(games))
	{
		teamA->details.MP++;
		teamB->details.MP++;

		teamA->details.GF += games->result->t1Goals;
		teamB->details.GF += games->result->t2Goals;

		teamA->details.GA += games->result->t2Goals;
		teamB->details.GA += games->result->t1Goals;

		teamA->details.GD = teamA->details.GF - teamA->details.GA;
		teamB->details.GD = teamB->details.GF - teamB->details.GA;

		teamA->details.Pts = 3 * teamA->details.W + teamA->details.D;
		teamB->details.Pts = 3 * teamB->details.W + teamB->details.D;
	}
	
	// after game set randomly a new lineup for teams
	setRandomLineup( teamA );
	setRandomLineup( teamB );
}
