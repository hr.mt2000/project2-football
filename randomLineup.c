#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "global.h"

static void selectPlayers( int n, player_t **players, size_t playersNum );

static int randPlayer( player_t **players, size_t num )
{
	int sum = 0;
	for( int i = 0; i < num; i++ )
	{
		sum += players[i]->skill;
	}
	int r = rand() % sum;
	int i = -1;
	while( r > 0 )
	{
		r -= players[++i]->skill;
	}
	return i;
}

// the first n players in players array are selected players // n is number of players to select
static void selectPlayers( int n, player_t **players, size_t playersNum )
{
	if( n == 0 )
		return;
		
	int ind = 0;
	player_t *tmp = NULL;
	ind = randPlayer( players, playersNum );
	tmp = players[0];
	players[0] = players[ind];
	players[ind] = tmp;
	selectPlayers( n-1, &players[1], playersNum-1 );
}

static int currentIndex = 0;
static void copyToMemory( player_t ***players, size_t playersNum, int *existPlayersNum, team_t *t )
{
	int x = MIN(playersNum, *existPlayersNum);
	if( x == 0 )
		return;
	memcpy( &t->lineup.players[currentIndex], *players, x * sizeof(player_t*) );
	(*existPlayersNum) -= x; // exist players decrease
	*players = &((*players)[x]);
	currentIndex += x;
}

void setRandomLineup( team_t *t )
{
	
	player_t **gols = NULL; // goalkeepers
	int golsNum = 0;
	player_t **defs = NULL; // defenders
	int defsNum = 0;
	player_t **mids = NULL; // midfielders
	int midsNum = 0;
	player_t **atts = NULL; // attackers
	int attsNum = 0;
	
	for( int i = 0; i < t->playersNumber; i++ )
	{
		switch( t->players[i].post )
		{
			case GOALKEEPER:
				gols = realloc(gols, (++golsNum) * sizeof(player_t*));
				gols[golsNum-1] = &t->players[i];
				break;
			case DEFENDER:
				defs = realloc(defs, (++defsNum) * sizeof(player_t*));
				defs[defsNum-1] = &t->players[i];
				break;
			case MIDFIELDER:
				mids = realloc(mids, (++midsNum) * sizeof(player_t*));
				mids[midsNum-1] = &t->players[i];
				break;
			case ATTACKER:
				atts = realloc(atts, (++attsNum) * sizeof(player_t*));
				atts[attsNum-1] = &t->players[i];
				break;
		}
	}
	
	
	formation_t *f = randForm();
	while( f->form[0] > defsNum || f->form[1] > midsNum || f->form[2] > attsNum )
		f = randForm();
	t->lineup.formation = f;
	
	
	short formGolsNum = 1, // we want just one goalkeeper
		  formDefsNum = f->form[0],
		  formMidsNum = f->form[1],
		  formAttsNum = f->form[2];
	
	
	selectPlayers( formGolsNum, gols, golsNum );
	selectPlayers( formDefsNum, defs, defsNum );
	selectPlayers( formMidsNum, mids, midsNum );
	selectPlayers( formAttsNum, atts, attsNum );
	
	currentIndex = 0;
	
	copyToMemory( &gols, formGolsNum, &golsNum, t ); // copy defenders
	copyToMemory( &defs, formDefsNum, &defsNum, t ); // copy defenders
	copyToMemory( &mids, formMidsNum, &midsNum, t ); // copy midfileders
	copyToMemory( &atts, formAttsNum, &attsNum, t ); // copy attackers
	copyToMemory( &gols, 1, &golsNum, t ); // copy subitutions
	copyToMemory( &defs, 2, &defsNum, t ); // copy subitutions
	copyToMemory( &mids, 2, &midsNum, t ); // copy subitutions
	copyToMemory( &atts, 2, &attsNum, t ); // copy subitutions
	copyToMemory( &gols, golsNum, &golsNum, t ); // copy other
	copyToMemory( &defs, defsNum, &defsNum, t ); // copy other
	copyToMemory( &mids, midsNum, &midsNum, t ); // copy other
	copyToMemory( &atts, attsNum, &attsNum, t ); // copy other
}
