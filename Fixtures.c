#include "global.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
extern team_t teams[TEAMNUM];
extern user_t *users;
extern game_t *groupGames[GRPGMENUM];
extern game_t *knockoutGames[KNKGMENUM];
void groupStage() ;
void knockoutStage() ;
void printGS(int j ,int i, int t1, int t2);

void fixtures() {
  int select = 0;
  while (select != 1 && select != 2)
  {
    puts("Please tell us what stage you looking for by entering ID");
    puts("1. Group stage");
    puts("2. Knockout stage");
    printf("\tSelect : ");
    scanf("%d", &select);
  }
  if (select == 1)
    groupStage();
  if (select == 2)
    knockoutStage();
  printf("\n");
  return ;
}



void groupStage ()
{
  
    for ( int i = 0 ; i < 8 ; i++)
    {
        printf("\n\t\t\t\t\t Group %c\n\n" , i + 65);
        printGS(0, i, 0, 1);
        printGS(0, i, 2, 3);
        printGS(1 ,i, 0, 2);
        printGS(1 ,i, 3, 1);
        printGS(2 ,i, 3, 0);
        printGS(2 ,i, 1, 2);
    }
    
}
void knockoutStage() 
{
    printf("\t\t\t\t      Round of 16\n\n");
    if(knockoutGames[0]->team1 == NULL)
    {
      printf("\t\t\t1) Winners Group A  -  Runners-up Group B\n");
      printf("\t\t\t2) Winners Group C  -  Runners-up Group D\n");
      printf("\t\t\t3) Winners Group E  -  Runners-up Group F\n");
      printf("\t\t\t4) Winners Group G  -  Runners-up Group H\n");
      printf("\t\t\t5) Winners Group B  -  Runners-up Group A\n");
      printf("\t\t\t6) Winners Group D  -  Runners-up Group C\n");
      printf("\t\t\t7) Winners Group F  -  Runners-up Group E\n");
      printf("\t\t\t8) Winners Group H  -  Runners-up Group G\n");
    }
    else
    {
      game_t *onGame = knockoutGames[0];
      for ( int n = 0 ; n < 8 ; n++)
      {
        short G1 = ' ', G2 = ' ';
        char *team1 = calloc(40, sizeof(char));
        memset(team1, ' ', 40);
        int size = strlen(onGame->team1->name);
        strcpy(&team1[39 - size], onGame->team1->name);
        if (onGame->result != NULL) 
        {
          G1 = onGame->result->t1Goals + '0';
          G2 = onGame->result->t2Goals + '0';
        }
        printf("%s  %c - %c  %s\n", team1, G1, G2, onGame->team2->name);
        free(team1);
        onGame = onGame->simGame ;
      }
    }
    printf("\n\t\t\t\t     Quarter Finals\n\n");
    if (knockoutGames[1]->team1 == NULL)
    {
      printf("\t\t\t1) Winners Match 1  -  Winners Match 2\n");
      printf("\t\t\t2) Winners Match 3  -  Winners Match 4\n");
      printf("\t\t\t3) Winners Match 5  -  Winners Match 6\n");
      printf("\t\t\t4) Winners Match 7  -  Winners Match 8\n");
    }
    else
    {
      game_t *onGame = knockoutGames[1];
      for (int n = 0; n < 4; n++) {
        short G1 = ' ', G2 = ' ';
        char *team1 = calloc(40, sizeof(char));
        memset(team1, ' ', 40);
        int size = strlen(onGame->team1->name);
        strcpy(&team1[39 - size], onGame->team1->name);
        if (onGame->result != NULL) {
          G1 = onGame->result->t1Goals + '0';
          G2 = onGame->result->t2Goals + '0';
        }
        printf("%s  %c - %c  %s\n", team1, G1, G2, onGame->team2->name);
        free(team1);
        onGame = onGame->simGame;
      }
    }
    printf("\n\t\t\t\t      Semi Finals\n\n");
    if (knockoutGames[2]->team1 == NULL)
     {
      printf("\t\t\t1) Winners Match 1  -  Winners Match 2\n");
      printf("\t\t\t2) Winners Match 3  -  Winners Match 4\n");
     } 
    else 
    {
      game_t *onGame = knockoutGames[2];
      for (int n = 0; n < 2; n++) {
        short G1 = ' ', G2 = ' ';
        char *team1 = calloc(40, sizeof(char));
        memset(team1, ' ', 40);
        int size = strlen(onGame->team1->name);
        strcpy(&team1[39 - size], onGame->team1->name);
        if (onGame->result != NULL) {
          G1 = onGame->result->t1Goals + '0';
          G2 = onGame->result->t2Goals + '0';
        }
        printf("%s  %c - %c  %s\n", team1, G1, G2, onGame->team2->name);
        free(team1);
        onGame = onGame->simGame;
      }
    }
    printf("\n\t\t\t\t  Third place play off\n\n");
    if (knockoutGames[3]->team1 == NULL) {
      printf("\t\t\t    Losers Match 1  -  Losers Match 2\n");
    } else 
    {
      
        short G1 = ' ', G2 = ' ';
        char *team1 = calloc(40, sizeof(char));
        memset(team1, ' ', 40);
        int size = strlen(knockoutGames[3]->team1->name);
        strcpy(&team1[39 - size], knockoutGames[3]->team1->name);
        if (knockoutGames[3]->result != NULL) {
          G1 = knockoutGames[3]->result->t1Goals + '0';
          G2 = knockoutGames[3]->result->t2Goals + '0';
        }
        printf("%s  %c - %c  %s\n", team1, G1, G2, knockoutGames[3]->team2->name);
        free(team1); 
    }
    printf("\n\t\t\t\t\t  Final\n\n");
    if (knockoutGames[4]->team1 == NULL) 
    {
      printf("\t\t\t   Winners Match 1  -  Winners Match 2\n");
    } 
    else 
    {
      short G1 = ' ', G2 = ' ';
      char *team1 = calloc(40, sizeof(char));
      memset(team1, ' ', 40);
      int size = strlen(knockoutGames[4]->team1->name);
      strcpy(&team1[39 - size], knockoutGames[4]->team1->name);
      if (knockoutGames[4]->result != NULL) {
        G1 = knockoutGames[4]->result->t1Goals + '0';
        G2 = knockoutGames[4]->result->t2Goals + '0';
      }
      printf("%s  %c - %c  %s\n", team1, G1, G2, knockoutGames[4]->team2->name);
      free(team1);
    }
}
void printGS(int j ,int i ,int t1, int t2)
{
  char *team1 = calloc(40, sizeof(char));
  memset(team1 ,' ' ,40);
  char *team2 = calloc(40, sizeof(char));
  memset(team2, ' ', 40);
  int size = 0 ;
  short G1 = ' ' , G2 = ' ' ;
  if ( j == 0 )
  {
    game_t *on = groupGames[0] ;
    static int z = 0 ;
    
    for(int p = 0 ; p < (i*2)+(z%2) ; p++ )
    {
        on = on->simGame ;
    }
    z++ ;
    size = strlen(on->team1->name);
    strcpy(&team1[39 - size], on->team1->name);
    strcpy(team2 ,on->team2->name);
    if (on->result != NULL)
    {
      G1 = on->result->t1Goals + '0';
      G2 = on->result->t2Goals + '0';
    }
  }
  else if ( j == 1)
  {
    game_t *on = groupGames[1] ;
    static int z = 0 ;
    
    for(int p = 0 ; p < (i*2)+(z%2) ; p++ )
    {
        on = on->simGame ;
    }
    z++ ;
    size = strlen(on->team1->name);
    strcpy(&team1[39 - size], on->team1->name);
    strcpy(team2, on->team2->name);
    if (on->result != NULL)
    {
      G1 = on->result->t1Goals + '0';
      G2 = on->result->t2Goals + '0';
    }
    on = groupGames[1]->simGame ;
  }
  else
  {
    game_t *on = groupGames[2] ;
    static int z = 0 ;
    
    for(int p = 0 ; p < (i*2)+(z%2) ; p++ )
    {
        on = on->simGame ;
    }
    z++ ;
    size = strlen(on->team1->name);
    strcpy(&team1[39 - size], on->team1->name);
    strcpy(team2, on->team2->name);
    if (on->result != NULL)
    {
      G1 = on->result->t1Goals + '0';
      G2 = on->result->t2Goals + '0';
    }
    on = groupGames[2]->simGame ;
  }
  printf("%s  %c - %c  %s\n", team1, G1 , G2 , team2); //G1 va G2 gol haye team hast
  free(team1);
  free(team2);
}

