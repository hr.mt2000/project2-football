#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "global.h"

extern user_t *users;
extern short usersNumber;
extern short currentUser;

extern team_t teams[TEAMNUM];
extern group_t groups[GROUPNUM];
extern game_t playoff[PLAYOFFGAMES];

extern game_t *groupGames[GRPGMENUM];
extern game_t *knockoutGames[KNKGMENUM];
extern short gameStage;

extern formation_t allowedForms[ALLOWFORMS];

char pptoc( playerPost_t post )
{
	char chars[5] = { 'G', 'D', 'M', 'A', 'S' };
	return chars[post];
}
continent_t strtocont( const char *str )
{
	if( strcmp(str, "CONCACAF") == 0 )
		return CONCACAF;
	else if( strcmp(str, "UEFA") == 0 )
		return UEFA;
	else if( strcmp(str, "AFC") == 0 )
		return AFC;
	else if( strcmp(str, "CAF") == 0 )
		return CAF;
	else //if( strcmp(str, "CONMEBOL") )
		return CONMEBOL;
}
playerPost_t strtopost( const char *str )
{
	if( strcmp(str, "G") == 0 )
		return GOALKEEPER;
	else if( strcmp(str, "D") == 0 )
		return DEFENDER;
	else if( strcmp(str, "M") == 0 )
		return MIDFIELDER;
	else //if( strcmp(str, "A") == 0 )
		return ATTACKER;
}

formation_t *randForm()
{
	int r = rand() % ALLOWFORMS;
	return &allowedForms[r];
}

int teamInd( team_t *t )
{
	for( int i = 0; i < TEAMNUM; i++ )
	{
		if( &teams[i] == t )
			return i;
	}
	return -1;
}

int userInd( char *username )
{
	for( int i = 0; i < usersNumber; i++ )
	{
		if( strcmp( users[i].username, username ) == 0 )
			return i;
	}
	return -1;
}
user_t *teamUser( team_t *team )
{
	for( int i = 0; i < usersNumber; i++ )
	{
		if( users[i].team == team )
			return &users[i];
	}
	return NULL;
}
team_t *currUserTeam()
{
	user_t *curusr = &users[currentUser];
	return curusr->team;
}

char *inputString(FILE* fp)
{
	char *str;
	char c;
	size_t len = 0;
	while(1) 
	{
		c = getchar();
		if( c == '\n' || c == EOF || c == '\0' )
			break;
			
		if( len == 0 )
			str = calloc(++len, sizeof(char)); // size is start size
		else
			str = realloc(str, sizeof(char)*(++len));
		if( str == NULL )
			perror("Error in memory allocation for input string in inputString function");
			
		str[len-1] = c;
	}
	str = realloc(str, sizeof(char)*(++len));
	if( str == NULL )
			perror("Error in memory allocation for input string in inputString function");
	str[len-1] = '\0'; // end of string

	return str;
}

int currPlayerInd( team_t *t, player_t *p )
{
	for( int i = 0; i < t->playersNumber; i++ )
	{
		if( t->lineup.players[i] == p )
			return i;
	}
	return -1;
}

playerPost_t currPlayerPost( team_t *t, player_t *p )
{
	int i = currPlayerInd(t,p), j = 0;
	for( j = 0; j < 3 && i > 0; j++ )
	{
		i -= t->lineup.formation->form[j];
	}
	return j;
}

bool isKnockout( game_t *g )
{
	for( int i = 0; i < KNKGMENUM; i++ )
	{
		game_t *g1 = NULL;
		while( g1 != knockoutGames[i] )
		{
			if( g1 == NULL )
				g1 = knockoutGames[i]->simGame;
			else
				g1 = g1->simGame;
			if( g1 == g )
				return true;
		}
	}
	return false;
}

formation_t *getFormationByForm( short arr[3] )
{
	for( int i = 0; i < ALLOWFORMS; i++ )
	{
		if( memcmp( arr, allowedForms[i].form, 3 * sizeof(short) ) == 0 )
			return &allowedForms[i];
	}
	return NULL;
}

bool isTrueForm( short form[3] )
{
	return getFormationByForm( form ) != NULL;
}

team_t *knockWinner( game_t *g )
{
	if( g->result == NULL )
		return NULL; // this game have not result, so it isn`t done yet
	else
	{
		if( g->result->t1Goals > g->result->t2Goals )
			return g->team1; // team1 won
		else
			return g->team2; // team2 won
	}
}
team_t *knockLoser( game_t *g )
{
	if( knockWinner( g ) == g->team1 )
		return g->team2;
	else
		return g->team1;
}
