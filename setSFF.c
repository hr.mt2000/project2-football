#include <stdlib.h>
#include "global.h"

extern team_t teams[TEAMNUM];

static short int setRandomSkill ()
{
    short int skill1 = 0, skill2 = 0;
    skill1 = rand() % 100;
    if (skill1 <= 5)							//	5%
    {
        skill2 = rand() % 20 + 40;
        return skill2;
    }
    else if (skill1 > 5 && skill1 <= 20)		//	15%
    {
        skill2 = rand() % 10 + 85;
        return skill2;
    }
    else if (skill1 > 20 && skill1 <= 50)		//	30%
    {
        skill2 = rand() % 10 + 60;
        return skill2;
    }
    else										// 	50%
    {
        skill2 = rand() % 15 + 70;
        return skill2;
    }
}

static short int setRandomFitness ()
{
    short int fitness = 0;
    fitness = rand() % 15 + 80;
    return fitness;
}

static short int setRandomForm ()
{
    short int form = 0;
    form = rand() % 4 + 7;
    return form;
}

void setSFF()
{
    int i = 0, j = 0;

    for (i = 0; i < TEAMNUM; i++)
    {
        for (j = 0; j < teams[i].playersNumber; j++)
        {
            teams[i].players[j].skill = setRandomSkill();
            teams[i].players[j].fitness = setRandomFitness();
            teams[i].players[j].form = setRandomForm();
        }
    }
}

/// Description
/*

SKill:
	0  - 40		0%
	95 - 100	0%
	40 - 60		5%
	85 - 95		15%
	60 - 70		30%
	70 - 85		50%

Fitness:
	0  - 80		0%
	95 - 100	0%
	80 - 95		100%

Form:
	0  - 6		0%
	11 - 15		0%
	7  - 10		100%

*/
